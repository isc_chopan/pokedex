package com.chopan.pokedex.ui.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chopan.pokedex.data.model.PokemonModel
import com.chopan.pokedex.domain.GetPokemonUserCase
import com.google.android.gms.location.FusedLocationProviderClient
import kotlinx.coroutines.launch

class PokedexViewModel : ViewModel() {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    val pokemonModel = MutableLiveData<PokemonModel>()
    var idPokemon = 0
    var getPokemonUseCase = GetPokemonUserCase()

    fun getPokemon(){
        val range = 1..1017
        idPokemon = range.random()
        viewModelScope.launch {
            val result : PokemonModel? = getPokemonUseCase(idPokemon.toString())
            pokemonModel.postValue(result!!)
        }
    }
}
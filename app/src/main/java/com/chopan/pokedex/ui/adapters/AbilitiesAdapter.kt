package com.chopan.pokedex.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chopan.pokedex.data.model.AbilityModel
import com.chopan.pokedex.databinding.CardAbilityBinding
import com.squareup.picasso.Picasso

class AbilitiesAdapter(private val abilities: List<AbilityModel>): RecyclerView.Adapter<AbilitiesAdapter.ViewHolder>() {
    private lateinit var context : Context
    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val binding = CardAbilityBinding
            .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        context = viewGroup.context
        return ViewHolder(binding)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.binding.apply {
            txtAbility.text = abilities.get(position).ability.name
            if(abilities.get(position).is_hidden){
                txtAbility.text = txtAbility.text.toString() + " (Oculta)"
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = abilities!!.size

    inner class ViewHolder(val binding: CardAbilityBinding) : RecyclerView.ViewHolder(binding.root){
        init {

        }
    }
}
package com.chopan.pokedex.ui.view

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.content.res.Resources
import android.os.*
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.chopan.pokedex.databinding.ActivityMainBinding
import com.chopan.pokedex.ui.adapters.AbilitiesAdapter
import com.chopan.pokedex.ui.viewmodel.PokedexViewModel
import com.google.android.gms.location.*
import com.squareup.picasso.Picasso
import com.chopan.pokedex.R


@RequiresApi(Build.VERSION_CODES.S)
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val pokedexViewModel: PokedexViewModel by viewModels()
    private lateinit var abilitiesAdapter: AbilitiesAdapter
    private val PERMISSIONS_CODE = 1

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback

    private lateinit var vibrator: Vibrator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if(ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED) {
                getLocationUpdates()
            }else{
                // You can directly ask for the permission.
                requestPermissions()
            }
        vibrator = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val vibratorManager: VibratorManager = getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager
            vibratorManager.getDefaultVibrator()

        } else {
            getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        }

        pokedexViewModel.pokemonModel.observe(this) {
            println(it.name)
            Picasso.get().load(it.spritesModel.front_default).into(binding.imgPokemon)
            binding.txtName.text = it.name
            var types = it.typeModels.get(0).type.name
            for (i in 0..it.typeModels.size - 1) {
                if (i > 0) {
                    types = types + " - " + it.typeModels.get(i).type.name
                }
            }
            binding.txtTypes.text = types
            abilitiesAdapter = AbilitiesAdapter(it.abilities)
            binding.recyclerAbilities.apply {
                adapter = abilitiesAdapter
                layoutManager =
                    LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
            }

        }
        binding.btnNewPokemon.setOnClickListener {
            pokedexViewModel.getPokemon()
        }
    }
    private fun getLocationUpdates(){
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, 1000).apply {
            setMinUpdateDistanceMeters(10f)
            setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
            setWaitForAccurateLocation(true)
        }.build()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult.locations.isNotEmpty()) {
                    val location = locationResult.lastLocation
                    tryVibrate(VibrationEffect.Composition.PRIMITIVE_THUD)
                    showPokemonAlert()
                }
            }
        }
    }
    private fun showPokemonAlert(){
        val alertPermission: AlertDialog
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Pokemon")
        builder.setMessage(Resources.getSystem().getString(R.string.new_pokemon_message))
        builder.setPositiveButton("Aceptar",
            DialogInterface.OnClickListener { dialog, action ->
                pokedexViewModel.getPokemon()
            })

        alertPermission = builder.create()
        alertPermission.show()
    }
    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                null /* Looper */
            )
            return
        }

    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }
    private fun tryVibrate(effectId: Int) {
        val vibratePattern = longArrayOf(1000,500,1500)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createWaveform(vibratePattern, -1))
        } else {
            vibrator.vibrate(vibratePattern, -1)
        }
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    // start receiving location update when activity  visible/foreground
    override fun onResume() {
        super.onResume()
        startLocationUpdates()
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    println("permisos no concedidos")
                    //requestPermissions()
                    val alertPermission: AlertDialog
                    val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                    builder.setTitle("Ubicación necesaria")
                    builder.setMessage(getString(R.string.permission_request_message))
                    builder.setPositiveButton("Aceptar",
                        DialogInterface.OnClickListener { dialog, action ->
                            finish()
                        })

                    alertPermission = builder.create()
                    alertPermission.show()
                }
                return
            }
        }
    }

    fun requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)) {
            val alertPermission: AlertDialog
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setMessage(getString(R.string.permission_request))
            builder.setPositiveButton("Continuar",
                DialogInterface.OnClickListener { dialog, action ->
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ),
                        PERMISSIONS_CODE
                    )
                })
                .setNegativeButton("Cerrar App",
                    DialogInterface.OnClickListener { dialog, action ->
                        run {
                            finish()
                        }
                    })

            alertPermission = builder.create()
            alertPermission.show()
        }else{
            println("no rationale request needed")
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                PERMISSIONS_CODE
            )
        }
    }
}
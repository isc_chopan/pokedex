package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.GenerationViiiModel
import com.chopan.pokedex.data.model.IconsModel

data class GenerationViii(
    val iconsModel: IconsModel
)

fun GenerationViiiModel.toDomain() = GenerationViiiModel(icons)
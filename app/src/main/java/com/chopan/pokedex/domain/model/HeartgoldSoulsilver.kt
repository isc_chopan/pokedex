package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.HeartgoldSoulsilverModel

data class HeartgoldSoulsilver(
    val back_default: String?,
    val back_female: String?,
    val back_shiny: String?,
    val back_shiny_female: String?,
    val front_default: String?,
    val front_female: String?,
    val front_shiny: String?,
    val front_shiny_female: String?
)

fun HeartgoldSoulsilverModel.toDomain() = HeartgoldSoulsilverModel(back_default, back_female, back_shiny, back_shiny_female, front_default, front_female, front_shiny, front_shiny_female)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.BlackWhiteModel
import com.chopan.pokedex.data.model.GenerationVModel

data class GenerationV(
    val blackWhiteModel: BlackWhiteModel
)

fun GenerationVModel.toDomain() = GenerationVModel(blackWhiteModel)
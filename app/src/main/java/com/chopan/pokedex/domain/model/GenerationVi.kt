package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.GenerationViModel
import com.chopan.pokedex.data.model.OmegarubyAlphasapphireModel

data class GenerationVi(
    val omegarubyAlphasapphireModel: OmegarubyAlphasapphireModel,
    val xY: XY
)

fun GenerationViModel.toDomain() = GenerationViModel(omegarubyAlphasapphireModel, xY)
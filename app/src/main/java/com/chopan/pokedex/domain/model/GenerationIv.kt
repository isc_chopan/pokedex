package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.DiamondPearlModel
import com.chopan.pokedex.data.model.GenerationIvModel
import com.chopan.pokedex.data.model.HeartgoldSoulsilverModel

data class GenerationIv(
    val diamondPearlModel: DiamondPearlModel,
    val heartgoldSoulsilverModel: HeartgoldSoulsilverModel,
    val platinum: Platinum
)

fun GenerationIvModel.toDomain() = GenerationIvModel(diamondPearlModel, heartgoldSoulsilverModel, platinumModel)
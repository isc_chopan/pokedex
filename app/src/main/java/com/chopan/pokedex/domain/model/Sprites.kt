package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.OtherModel
import com.chopan.pokedex.data.model.SpritesModel


data class Sprites(
    val back_default: String,
    val back_female: Any,
    val back_shiny: String,
    val back_shiny_female: Any,
    val front_default: String,
    val front_female: Any,
    val front_shiny: String,
    val front_shiny_female: Any,
    val otherModel: OtherModel,
    val versions: Versions
)

fun SpritesModel.toDomain() = SpritesModel(back_default, back_female, back_shiny, back_shiny_female, front_default, front_female, front_shiny, front_shiny_female, otherModel, versionsModel)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.FireredLeafgreenModel

data class FireredLeafgreen(
    val back_default: String?,
    val back_shiny: String?,
    val front_default: String?,
    val front_shiny: String?
)

fun FireredLeafgreenModel.toDomain() = FireredLeafgreenModel(
    back_default,
    back_shiny,
    front_default,
    front_shiny
)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.GameIndiceModel

data class GameIndice(
    val game_index: Int,
    val version: Version
)

fun GameIndiceModel.toDomain() = GameIndiceModel(game_index, versionModel)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.EmeraldModel

data class Emerald(
    val front_default: String,
    val front_shiny: String
)

fun EmeraldModel.toDomain() = EmeraldModel(
    front_default,
    front_shiny
)
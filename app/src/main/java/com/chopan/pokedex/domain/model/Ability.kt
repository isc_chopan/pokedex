package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.AbilityModel
import com.google.gson.annotations.SerializedName

data class Ability(
    val ability: AbilityX,
    val is_hidden: Boolean,
    val slot: Int
)

fun AbilityModel.toDomain() = AbilityModel(ability, is_hidden, slot)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.StatModel


data class Stat(
    val base_stat: Int,
    val effort: Int,
    val stat: StatX
)

fun StatModel.toDomain() = StatModel(base_stat, effort, stat)
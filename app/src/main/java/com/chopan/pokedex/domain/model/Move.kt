package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.MoveModel


data class Move(
    val move: MoveX,
    val version_group_details: List<VersionGroupDetail>
)

fun MoveModel.toDomain() = MoveModel(move, version_group_details)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.*

data class Versions(
    val generationIModel: GenerationIModel,
    val generationIIIModel: GenerationIiiModel,
    val generationIVModel: GenerationIvModel,
    val generationVModel: GenerationVModel,
    val generationVIModel: GenerationViModel,
    val generationVIIModel: GenerationViiModel,
    val generationVIIIModel: GenerationViiiModel
)

fun VersionsModel.toDomain() = VersionsModel(generationIModel, generationIIIModel, generationIVModel, generationVModel, generationVIModel, generationVIIModel, generationVIIIModel)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.ItemModel


data class Item(
    val name: String,
    val url: String
)

fun ItemModel.toDomain() = ItemModel(name, url)
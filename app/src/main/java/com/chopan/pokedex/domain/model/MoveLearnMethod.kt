package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.MoveLearnMethodModel
import com.google.gson.annotations.SerializedName

data class MoveLearnMethod(
    val name: String,
    val url: String
)

fun MoveLearnMethodModel.toDomain() = MoveLearnMethodModel(name, url)
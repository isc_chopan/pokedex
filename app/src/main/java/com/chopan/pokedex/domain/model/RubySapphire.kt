package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.RubySapphireModel


data class RubySapphire(
    val back_default: String,
    val back_shiny: String,
    val front_default: String,
    val front_shiny: String
)

fun RubySapphireModel.toDomain() = RubySapphireModel(back_default, back_shiny, front_default, front_shiny)
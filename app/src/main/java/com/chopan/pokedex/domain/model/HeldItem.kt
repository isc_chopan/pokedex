package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.HeldItemModel

data class HeldItem(
    val item: Item,
    val version_details: List<VersionDetail>
)

fun HeldItemModel.toDomain() = HeldItemModel(itemModel, version_details)
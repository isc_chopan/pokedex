package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.VersionDetailModel
import com.chopan.pokedex.data.model.VersionModel

data class VersionDetail(
    val rarity: Int,
    val versionModel: VersionModel
)

fun VersionDetailModel.toDomain() = VersionDetailModel(rarity, versionModel)
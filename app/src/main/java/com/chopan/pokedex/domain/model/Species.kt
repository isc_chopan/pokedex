package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.SpeciesModel


data class Species(
    val name: String,
    val url: String
)

fun SpeciesModel.toDomain() = SpeciesModel(name, url)
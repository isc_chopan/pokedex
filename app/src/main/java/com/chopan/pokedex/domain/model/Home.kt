package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.HomeModel

data class Home(
    val front_default: String,
    val front_female: Any,
    val front_shiny: String,
    val front_shiny_female: Any
)

fun HomeModel.toDomain() = HomeModel(front_default, front_female, front_shiny, front_shiny_female)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.TypeXModel


data class TypeX(
    val name: String,
    val url: String
)

fun TypeXModel.toDomain() = TypeXModel(name, url)
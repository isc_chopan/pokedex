package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.GoldModel
import com.google.gson.annotations.SerializedName

data class Gold(
    val back_default: String,
    val back_shiny: String,
    val front_default: String,
    val front_shiny: String,
    val front_transparent: String
)

fun GoldModel.toDomain() = GoldModel(back_default, back_shiny, front_default, front_shiny, front_transparent)
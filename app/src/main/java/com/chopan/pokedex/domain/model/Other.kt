package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.DreamWorldModel
import com.chopan.pokedex.data.model.HomeModel
import com.chopan.pokedex.data.model.OfficialArtworkModel
import com.chopan.pokedex.data.model.OtherModel

data class Other(
    val dream_world: DreamWorldModel,
    val homeModel: HomeModel,
    val officialArtworkModel: OfficialArtworkModel
)

fun OtherModel.toDomain() = OtherModel(dream_world, homeModel, officialArtworkModel)
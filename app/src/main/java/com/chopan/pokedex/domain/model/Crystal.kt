package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.CrystalModel

data class Crystal(
    val back_default: String,
    val back_shiny: String,
    val back_shiny_transparent: String,
    val back_transparent: String,
    val front_default: String,
    val front_shiny: String,
    val front_shiny_transparent: String,
    val front_transparent: String
)

fun CrystalModel.toDomain() = CrystalModel(
    back_default,
    back_shiny,
    back_shiny_transparent,
    back_transparent,
    front_default,
    front_shiny,
    front_shiny_transparent,
    front_transparent
)
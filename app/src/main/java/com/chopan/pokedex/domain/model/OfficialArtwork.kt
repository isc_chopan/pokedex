package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.OfficialArtworkModel


data class OfficialArtwork(
    val front_default: String,
    val front_shiny: String
)

fun OfficialArtworkModel.toDomain() = OfficialArtworkModel(front_default, front_shiny)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.CrystalModel
import com.chopan.pokedex.data.model.GenerationIiModel

data class GenerationIi(
    val crystalModel: CrystalModel,
    val gold: Gold,
    val silver: Silver
)

fun GenerationIiModel.toDomain() = GenerationIiModel(crystalModel, goldModel, silverModel)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.YellowModel

data class Yellow(
    val back_default: Any,
    val back_gray: Any,
    val back_transparent: Any,
    val front_default: Any,
    val front_gray: Any,
    val front_transparent: Any
)

fun YellowModel.toDomain() = YellowModel(back_default, back_gray, back_transparent, front_default, front_gray, front_transparent)
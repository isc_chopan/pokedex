package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.VersionGroupDetailModel


data class VersionGroupDetail(
    val level_learned_at: Int,
    val move_learn_method: MoveLearnMethod,
    val version_group: VersionGroup
)

fun VersionGroupDetailModel.toDomain() = VersionGroupDetailModel(level_learned_at, move_learn_method, version_group)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.XYModel


data class XY(
    val front_default: String?,
    val front_female: String?,
    val front_shiny: String?,
    val front_shiny_female: String?
)

fun XYModel.toDomain() = XYModel(front_default, front_female, front_shiny, front_shiny_female)
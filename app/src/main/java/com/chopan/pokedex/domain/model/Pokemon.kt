package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.*

data class Pokemon(
    val abilities: List<AbilityModel>,
    val base_experience: Int,
    val formModels: List<FormModel>,
    val game_indices: List<GameIndiceModel>,
    val height: Int,
    val held_items: List<HeldItemModel>,
    val id: Int,
    val is_default: Boolean,
    val location_area_encounters: String,
    val moveModels: List<MoveModel>,
    val name: String,
    val order: Int,
    val past_abilities: List<Any>,
    val past_types: List<Any>,
    val species: Species,
    val sprites: Sprites,
    val stats: List<Stat>,
    val types: List<Type>,
    val weight: Int
)

fun PokemonModel.toDomain() = PokemonModel(abilities, base_experience, formModels, game_indices, height, held_items, id, is_default, location_area_encounters, moveModels, name, order, past_abilities, past_types, speciesModel, spritesModel, statModels, typeModels, weight)
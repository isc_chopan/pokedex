package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.TypeModel


data class Type(
    val slot: Int,
    val type: TypeX
)

fun TypeModel.toDomain() = TypeModel(slot, type)
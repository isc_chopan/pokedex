package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.MoveXModel


data class MoveX(
    val name: String,
    val url: String
)

fun MoveXModel.toDomain() = MoveXModel(name, url)
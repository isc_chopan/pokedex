package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.IconsModel


data class Icons(
    val front_default: String,
    val front_female: Any
)

fun IconsModel.toDomain() = IconsModel(front_default, front_female)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.DreamWorldModel
import com.google.gson.annotations.SerializedName

data class DreamWorld(
    val front_default: String,
    val front_female: Any
)

fun DreamWorldModel.toDomain() = DreamWorldModel(
    front_default,
    front_female
)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.DiamondPearlModel

data class DiamondPearl(
    val back_default: String,
    val back_female: Any,
    val back_shiny_female: Any,
    val back_shiny: String,
    val front_default: String,
    val front_female: Any,
    val front_shiny: String,
    val front_shiny_female: Any
)

fun DiamondPearlModel.toDomain() = DiamondPearlModel(
    back_default,
    back_female,
    back_shiny_female,
    back_shiny,
    front_default,
    front_female,
    front_shiny,
    front_shiny_female
)
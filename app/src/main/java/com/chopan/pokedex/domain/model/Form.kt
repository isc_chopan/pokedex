package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.FormModel

data class Form(
    val name: String,
    val url: String
)

fun FormModel.toDomain() = FormModel(name, url)
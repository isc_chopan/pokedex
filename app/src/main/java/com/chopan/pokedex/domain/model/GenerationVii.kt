package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.GenerationViiModel
import com.chopan.pokedex.data.model.IconsModel
import com.chopan.pokedex.data.model.UltraSunUltraMoonModel

data class GenerationVii(
    val iconsModel: IconsModel,
    val ultraSunUltraMoonModel: UltraSunUltraMoonModel
)

fun GenerationViiModel.toDomain() = GenerationViiModel(icons, ultraSunUltraMoon)
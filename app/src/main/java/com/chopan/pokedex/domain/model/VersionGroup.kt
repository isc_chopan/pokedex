package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.VersionGroupModel


data class VersionGroup(
    val name: String,
    val url: String
)

fun VersionGroupModel.toDomain() = VersionGroupModel(name, url)
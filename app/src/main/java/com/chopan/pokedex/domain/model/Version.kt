package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.VersionModel


data class Version(
    val name: String,
    val url: String
)

fun VersionModel.toDomain() = VersionModel(name, url)
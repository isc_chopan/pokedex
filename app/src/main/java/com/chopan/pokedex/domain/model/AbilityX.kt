package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.AbilityModel
import com.chopan.pokedex.data.model.AbilityXModel
import com.google.gson.annotations.SerializedName

data class AbilityX(
    val name: String,
    val url: String
)

fun AbilityXModel.toDomain() = AbilityXModel(name, url)
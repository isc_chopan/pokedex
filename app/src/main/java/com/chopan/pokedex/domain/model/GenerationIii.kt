package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.EmeraldModel
import com.chopan.pokedex.data.model.FireredLeafgreenModel
import com.chopan.pokedex.data.model.GenerationIiiModel

data class GenerationIii(
    val emeraldModel: EmeraldModel,
    val fireredLeafgreenModel: FireredLeafgreenModel,
    val rubySapphire: RubySapphire
)

fun GenerationIiiModel.toDomain() = GenerationIiiModel(emeraldModel, fireredLeafgreenModel, rubySapphireModel)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.OmegarubyAlphasapphireModel

data class OmegarubyAlphasapphire(
    val front_default: String,
    val front_female: Any,
    val front_shiny: String,
    val front_shiny_female: Any
)

fun OmegarubyAlphasapphireModel.toDomain() = OmegarubyAlphasapphireModel(front_default, front_female, front_shiny, front_shiny_female)
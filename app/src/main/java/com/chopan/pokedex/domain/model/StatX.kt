package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.StatXModel


data class StatX(
    val name: String,
    val url: String
)

fun StatXModel.toDomain() = StatXModel(name, url)
package com.chopan.pokedex.domain.model

import com.chopan.pokedex.data.model.GenerationIModel

data class GenerationI(
    val redBlue: RedBlue,
    val yellow: Yellow
)

fun GenerationIModel.toDomain() = GenerationIModel(redBlueModel, yellowModel)
package com.chopan.pokedex.domain

import com.chopan.pokedex.data.PokemonRepository
import com.chopan.pokedex.data.model.PokemonModel

class GetPokemonUserCase {
    private val repository = PokemonRepository()

    suspend operator fun invoke(id: String): PokemonModel? = repository.getPokemon(id)
}
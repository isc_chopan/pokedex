package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class HeldItemModel(
    @SerializedName("item")val itemModel: ItemModel,
    @SerializedName("version_details")val version_details: List<VersionDetailModel>
)
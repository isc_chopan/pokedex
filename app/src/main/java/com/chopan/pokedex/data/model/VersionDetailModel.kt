package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class VersionDetailModel(
    @SerializedName("rarity")val rarity: Int,
    @SerializedName("version")val versionModel: VersionModel
)
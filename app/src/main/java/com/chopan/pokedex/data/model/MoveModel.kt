package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class MoveModel(
    @SerializedName("move")val move: MoveXModel,
    @SerializedName("version_group_details")val version_group_details: List<VersionGroupDetailModel>
)
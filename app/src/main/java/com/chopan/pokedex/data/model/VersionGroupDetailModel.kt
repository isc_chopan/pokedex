package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class VersionGroupDetailModel(
    @SerializedName("level_learned_at")val level_learned_at: Int,
    @SerializedName("move_learn_method")val move_learn_method: MoveLearnMethodModel,
    @SerializedName("version_group")val version_group: VersionGroupModel
)
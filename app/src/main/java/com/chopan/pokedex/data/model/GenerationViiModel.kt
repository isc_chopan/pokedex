package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class GenerationViiModel(
    @SerializedName("icons")val icons: IconsModel,
    @SerializedName("ultra-sun-ultra-moon")val ultraSunUltraMoon: UltraSunUltraMoonModel
)
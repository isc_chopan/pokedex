package com.chopan.pokedex.data

import com.chopan.pokedex.data.model.PokemonModel
import com.chopan.pokedex.data.network.PokemonService

class PokemonRepository {
    private val api = PokemonService()

    suspend fun getPokemon(id:String):PokemonModel? {
        val response = api.getPokemon(id)
        return response
    }
}
package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class GenerationIvModel(
    @SerializedName("diamond-pearl")val diamondPearlModel: DiamondPearlModel,
    @SerializedName("heartgold-soulsilver")val heartgoldSoulsilverModel: HeartgoldSoulsilverModel,
    @SerializedName("platinum")val platinumModel: PlatinumModel
)
package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class GenerationViModel(
    @SerializedName("omegaruby-alphasapphire")val omegarubyAlphasapphireModel: OmegarubyAlphasapphireModel,
    @SerializedName("x-y")val xY: XYModel
)
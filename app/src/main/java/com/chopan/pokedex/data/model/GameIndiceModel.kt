package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class GameIndiceModel(
    @SerializedName("game_index")val game_index: Int,
    @SerializedName("version")val versionModel: VersionModel
)
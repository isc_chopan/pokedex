package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class GenerationIiModel(
    @SerializedName("crystal")val crystalModel: CrystalModel,
    @SerializedName("gold")val goldModel: GoldModel,
    @SerializedName("silver")val silverModel: SilverModel
)
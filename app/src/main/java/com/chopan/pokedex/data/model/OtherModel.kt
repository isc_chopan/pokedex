package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class OtherModel(
    @SerializedName("dream_world")val dream_world: DreamWorldModel,
    @SerializedName("home")val homeModel: HomeModel,
    @SerializedName("official-artwork")val officialArtworkModel: OfficialArtworkModel
)
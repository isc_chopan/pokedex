package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class GenerationViiiModel(
    @SerializedName("icons")val icons: IconsModel
)
package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class TypeXModel(
    @SerializedName("name")val name: String,
    @SerializedName("url")val url: String
)
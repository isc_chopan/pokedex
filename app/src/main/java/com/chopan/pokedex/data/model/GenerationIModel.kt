package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class GenerationIModel(
    @SerializedName("red-blue")val redBlueModel: RedBlueModel,
    @SerializedName("yellow")val yellowModel: YellowModel
)
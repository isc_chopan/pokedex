package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class FireredLeafgreenModel(
    @SerializedName("back_default")val back_default: Any,
    @SerializedName("back_shiny")val back_shiny: Any,
    @SerializedName("front_default")val front_default: Any,
    @SerializedName("front_shiny")val front_shiny: Any
)
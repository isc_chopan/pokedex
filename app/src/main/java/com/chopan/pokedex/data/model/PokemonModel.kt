package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class PokemonModel(
    @SerializedName("abilities")val abilities: List<AbilityModel>,
    @SerializedName("base_experience")val base_experience: Int,
    @SerializedName("forms")val formModels: List<FormModel>,
    @SerializedName("game_indices")val game_indices: List<GameIndiceModel>,
    @SerializedName("height")val height: Int,
    @SerializedName("held_items")val held_items: List<HeldItemModel>,
    @SerializedName("id")val id: Int,
    @SerializedName("is_default")val is_default: Boolean,
    @SerializedName("location_area_encounters")val location_area_encounters: String,
    @SerializedName("moves")val moveModels: List<MoveModel>,
    @SerializedName("name")val name: String,
    @SerializedName("order")val order: Int,
    @SerializedName("past_abilities")val past_abilities: List<Any>,
    @SerializedName("past_types")val past_types: List<Any>,
    @SerializedName("species")val speciesModel: SpeciesModel,
    @SerializedName("sprites")val spritesModel: SpritesModel,
    @SerializedName("stats")val statModels: List<StatModel>,
    @SerializedName("types")val typeModels: List<TypeModel>,
    @SerializedName("weight")val weight: Int
)
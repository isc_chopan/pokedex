package com.chopan.pokedex.data.model

data class UltraSunUltraMoonModel(
    val front_default: String,
    val front_female: Any,
    val front_shiny: String,
    val front_shiny_female: Any
)
package com.chopan.pokedex.data.network

import com.chopan.pokedex.data.model.PokemonModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface PokemonApiClient {

    @GET("pokemon/{id}")
    suspend fun getPokemon(@Path("id") id: String):Response<PokemonModel>
}
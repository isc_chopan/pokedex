package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class GenerationIiiModel(
    @SerializedName("red-crystal")val emeraldModel: EmeraldModel,
    @SerializedName("firered-leafgreen")val fireredLeafgreenModel: FireredLeafgreenModel,
    @SerializedName("ruby-sapphire")val rubySapphireModel: RubySapphireModel
)
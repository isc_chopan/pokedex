package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class VersionGroupModel(
    @SerializedName("name")val name: String,
    @SerializedName("url")val url: String
)
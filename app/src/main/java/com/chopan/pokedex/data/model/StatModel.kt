package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class StatModel(
    @SerializedName("base_stat")val base_stat: Int,
    @SerializedName("effort")val effort: Int,
    @SerializedName("stat")val stat: StatXModel
)
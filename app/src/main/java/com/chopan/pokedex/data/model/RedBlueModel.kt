package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class RedBlueModel(
    @SerializedName("back_default")val back_default: Any,
    @SerializedName("back_gray")val back_gray: Any,
    @SerializedName("back_transparent")val back_transparent: Any,
    @SerializedName("front_default")val front_default: Any,
    @SerializedName("front_gray")val front_gray: Any,
    @SerializedName("front_transparent")val front_transparent: Any
)
package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class AbilityModel(
    @SerializedName("ability")val ability: AbilityXModel,
    @SerializedName("is_hidden")val is_hidden: Boolean,
    @SerializedName("slot")val slot: Int
)
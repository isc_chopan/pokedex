package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class DreamWorldModel(
    @SerializedName("front_default")val front_default: String,
    @SerializedName("front_female")val front_female: Any
)
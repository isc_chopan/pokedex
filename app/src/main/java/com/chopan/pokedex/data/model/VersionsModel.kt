package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class VersionsModel(
    @SerializedName("generation-i")val generationIModel: GenerationIModel,
    @SerializedName("generation-iii")val generationIIIModel: GenerationIiiModel,
    @SerializedName("generation-iv")val generationIVModel: GenerationIvModel,
    @SerializedName("generation-v")val generationVModel: GenerationVModel,
    @SerializedName("generation-vi")val generationVIModel: GenerationViModel,
    @SerializedName("generation-vii")val generationVIIModel: GenerationViiModel,
    @SerializedName("generation-viii")val generationVIIIModel: GenerationViiiModel
)
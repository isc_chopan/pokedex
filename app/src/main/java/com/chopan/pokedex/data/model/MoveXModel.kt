package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class MoveXModel(
    @SerializedName("name")val name: String,
    @SerializedName("url")val url: String
)
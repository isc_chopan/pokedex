package com.chopan.pokedex.data.model

import com.google.gson.annotations.SerializedName

data class TypeModel(
    @SerializedName("slot")val slot: Int,
    @SerializedName("type")val type: TypeXModel
)
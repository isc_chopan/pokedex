package com.chopan.pokedex.data.network

import com.chopan.pokedex.core.RetrofitHelper
import com.chopan.pokedex.data.model.PokemonModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class PokemonService {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getPokemon(id: String):PokemonModel?{
        return withContext(Dispatchers.IO){
            val response : Response<PokemonModel> = retrofit.create(PokemonApiClient::class.java).getPokemon(id)
            response.body()
        }
    }
}